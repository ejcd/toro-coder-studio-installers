#!/bin/sh
set -e
BambooCoderStudioVer="$1"
CoderStudioBranch="$2"

case "$IntegrateBranch" in
	develop)
	echo "[ INFO ] This is a develop deployment."
	echo "[ INFO ] Defining path ..."
	PATHDEV=/datastore/clients/dev-server/download_dev/coderstudio_dev_builds
	;;
	release)
	echo "[ INFO ] This is a release deployment."
	echo "[ INFO ] Defining path ..."
	PATHDEV=/datastore/clients/dev-server/download_dev/coderstudio_dev_builds
	;;
	production)
	echo "[ INFO ] This is a production deployment."
	echo "[ INFO ] Defining path ..."
	#PATHDEV=/datastore/clients/dev-server/download_dir/integrate
	;;
	*)
	echo "[ Error] Missing parameter. Review deployment tasks."
esac

echo "..... Starting Installer Script ....."
#requirements:
# - install4j app
# - the install4j project files
# - configure location of file on Install4j
# - coder studio files

#path to install4jc
IJ4C=/opt/apps/install4j-latest/bin/install4jc

#location project file for installers
PROJ_FILE=./coder-studio-win.install4j

echo "..... You are in $(pwd) ...."

echo "..... Creating temporary folders ....."
mkdir -p coder-studio-installers

echo "..... Copying Install4J Configuration ....."
cp /opt/apps/toro-coder-studio-installers/coder-studio-win.install4j ./

echo "..... Getting Version of TORO Integrate ....."
ARTIFACT=io.toro.coder.product-win32.win32.x86.zip
FILENAME=toro-coder-studio-windows_x86


echo "..... Variables ....."
echo "artifact=$ARTIFACT"
echo "filename=$FILENAME"
echo "version=$BambooCoderStudioVer"

echo "..... Extracting TORO Integrate ....."
unzip -qo $ARTIFACT -d coder-studio-installers/$FILENAME

#compile the installers
echo "=>> Compiling Installers ..."
$IJ4C $PROJ_FILE -r $version -v --win-keystore-password=Va3X}\'UG

#end
echo "Done Building Installers!"


echo "=>> Cleaning Temporary Folders ..."
rm -rf coder-studio-installers
